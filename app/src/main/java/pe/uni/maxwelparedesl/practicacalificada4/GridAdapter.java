package pe.uni.maxwelparedesl.practicacalificada4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> title;
    ArrayList<String> description;
    ArrayList<Integer> image;

    public GridAdapter(Context context, ArrayList<String> title, ArrayList<String> description, ArrayList<Integer> image) {
        this.context = context;
        this.title= title;
        this.description = description;
        this.image = image;
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout, parent, false);
        TextView textViewDishTitle = view.findViewById(R.id.text_view_dish_title);
        TextView textViewDesciption = view.findViewById(R.id.text_view_dish_description);
        ImageView imageViewDish = view.findViewById(R.id.image_view_dish);
        imageViewDish.setImageResource(image.get(position));
        textViewDesciption.setText(description.get(position));
        textViewDishTitle.setText(title.get(position));
        return view;
    }
}
