package pe.uni.maxwelparedesl.practicacalificada4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);
        rellenarArrays();

        GridAdapter gridAdapter = new GridAdapter(this,title,description,image);


        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MainActivity.this,SecondActivity.class);
            intent.putExtra("dish",title.get(position));
            startActivity(intent);
        });

    }

    private void rellenarArrays() {
        title.add("Ceviche");
        title.add("Lomo");
        title.add("Chicharon");
        title.add("Relleno");
        title.add("Tallarines");
        title.add("Pollo");
        title.add("Olluco");
        title.add("Causa");

        description.add("Platillo a base de pescado crudo marinado, ajíes y especias");
        description.add("Platillo a base de carne y papas fritas");
        description.add("Platillo chicarron de cerdo");
        description.add("Platillo a base de relleno de carne");
        description.add("Platillo a base de fideos");
        description.add("Platillo de Pollo a la brasa");
        description.add("Platillo a base de olluco con carne o pollo");
        description.add("Platillo a base de papa amarilla con relleno de pollo");

        image.add(R.drawable.ceviche);
        image.add(R.drawable.lomo);
        image.add(R.drawable.chicharon);
        image.add(R.drawable.relleno);
        image.add(R.drawable.tallarines2);
        image.add(R.drawable.brasa);
        image.add(R.drawable.olluco);
        image.add(R.drawable.causa);
    }
}