package pe.uni.maxwelparedesl.practicacalificada4;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class SecondActivity extends AppCompatActivity {

    TextView textViewDish;
    Spinner spinnerDish;
    LinearLayout linearLayout;
    ArrayAdapter<CharSequence> adapter;
    EditText editTextNameDestinatario, editTextAdressDestinatario;
    RadioGroup radioGroup;
    RadioButton radioButtonVisa;
    RadioButton radioButtonEfectivo;
    Button buttonEnviarPerdido;
    SharedPreferences sharedPreferences;
    String nombre,direccion, cantidad;
    boolean visa, efectivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        editTextNameDestinatario = findViewById(R.id.nombre_destinatario);
        editTextAdressDestinatario = findViewById(R.id.direccion_destinatario);
        radioGroup = findViewById(R.id.radio_group);
        radioButtonVisa = findViewById(R.id.radio_button_visa);
        radioButtonEfectivo = findViewById(R.id.radio_button_efectivo);
        buttonEnviarPerdido = findViewById(R.id.button);
        spinnerDish = findViewById(R.id.spinner_1);
        textViewDish = findViewById(R.id.plato_seleccionado);

        Intent intent = getIntent();

        String platoSeleccionado = intent.getStringExtra("dish");

        textViewDish.setText(platoSeleccionado);

        adapter = ArrayAdapter.createFromResource(this, R.array.dishes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDish.setAdapter(adapter);


        buttonEnviarPerdido.setOnClickListener(v-> {
             //   Snackbar.make(linearLayout,"Por Favor llenar todos los campos",Snackbar.LENGTH_INDEFINITE).setAction("OK",vo->{}).show();
        });


        infoRecuperada();
    }

    @Override
    protected void onPause() {
        super.onPause();
        guardarInfo();
    }

    private void guardarInfo() {

        sharedPreferences = getSharedPreferences("guardarInfo", Context.MODE_PRIVATE);
        nombre = editTextNameDestinatario.getText().toString();
        direccion = editTextAdressDestinatario.getText().toString();
        visa = radioButtonVisa.isChecked();
        efectivo = radioButtonEfectivo.isChecked();

        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nombre", nombre);
        editor.putString("direccion", direccion);
        editor.putBoolean("visa", visa);
        editor.putBoolean("efectivo", efectivo);
        editor.apply();

    }
    private void infoRecuperada() {
        sharedPreferences = getSharedPreferences("guardarInfo", Context.MODE_PRIVATE);
        nombre = sharedPreferences.getString("nombre",null);
        direccion = sharedPreferences.getString("direccion",null);
        visa = sharedPreferences.getBoolean("visa",false);
        efectivo = sharedPreferences.getBoolean("efectivo",false);
        editTextNameDestinatario.setText(nombre);
        editTextAdressDestinatario.setText(direccion);
        radioButtonVisa.setChecked(visa);
        radioButtonEfectivo.setChecked(efectivo);

    }

}